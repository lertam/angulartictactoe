var app = angular.module('app', []);
var firstStep = "1 (X)";
var secondStep = "2 (O)";
app.directive('field', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			
			element.on('click', function(){
				if(scope.board[attrs.x][attrs.y] == 0 && scope.rest > 0){
					if(scope.last === 'cross'){
						element.addClass('round');
						scope.last = 'round';
						scope.board[attrs.x][attrs.y] = 2;
					} else {
						element.addClass('cross');
						scope.last = 'cross';
						scope.board[attrs.x][attrs.y] = 1;
					}
					scope.rest -= 1;
					scope.checkBoard();
					console.log(scope.rest);
				}
			});

		}
	};
});

app.controller('myCtrl', function($scope) {
	$scope.board = [
		[0, 0, 0],
		[0, 0, 0],
		[0, 0, 0]];
	$scope.last = "round";
	$scope.step = firstStep;
	$scope.rest = 9;
	$scope.checkBoard = function(){
		// Проверка всех по горизонтали
		for(var i = 0; i < 3; i++){
			var cross = 0;
			var round = 0;
			for(var j = 0; j < 3; j++){
				if ($scope.board[i][j] === 1) cross++;
				else if($scope.board[i][j] == 2) round++;
			}
			if (cross === 3){
				$scope.win(1);
				return;
			}
			if (round === 3){
				$scope.win(2);
				return;
			}
		}
		console.log('По горизонтали победителей нет.');
		// По вертикали
		for(var i = 0; i < 3; i++){
			var cross = 0;
			var round = 0;
			for(var j = 0; j < 3; j++){
				if ($scope.board[j][i] === 1) cross++;
				else if($scope.board[j][i] == 2) round++;
			}
			if (cross === 3){
				$scope.win(1);
				return;
			}
			if (round === 3){
				$scope.win(2);
				return;
			}
		}
		console.log('По вертикали победителей нет.');
		// Диагональ 1
		var cross = 0;
		var round = 0;
		for(var i = 0; i < 3; i++){
			if ($scope.board[i][i] === 1) cross++;
			else if($scope.board[i][i] == 2) round++;
		}
		if (cross === 3){
			$scope.win(1);
			return;
		}
		if (round === 3){
			$scope.win(2);
			return;
		}
		console.log('По диагонали 1 победителей нет.');
		// Диагональ 2
		var cross = 0;
		var round = 0;
		for(var i = 0; i < 3; i++){
			if ($scope.board[i][2 - i] === 1) cross++;
			else if($scope.board[i][2 - i] == 2) round++;
		}
		if (cross === 3){
			$scope.win(1);
			return;
		}
		if (round === 3){
			$scope.win(2);
			return;
		}
		console.log('По диагонали 2 победителей нет.');
		if ($scope.rest == 0){
			$scope.win(3);
		}
	};
	$scope.win = function(winner) {
		$scope.winner = 'Победитель - ';
		if (winner === 1) $scope.winner += firstStep;
		else if(winner === 2) $scope.winner += secondStep;
		else if (winner === 3) $scope.winner = 'Ничья!';
		alert($scope.winner);
		$scope.rest = 0;
	}
});